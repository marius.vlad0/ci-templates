# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:

# We can not do multi-level includes, so we need to manually include all of
# our templates here
include:
  # projects using these templates should not need to pull the bootstrap
  - local: '/bootstrap/bootstrap.yml'

  # Alpine container builder template
  # projects using this should reference this with the following:
  #
  # - project: 'wayland/ci-templates'
  #   ref: master # or git sha, see https://docs.gitlab.com/ee/ci/yaml/#includefile
  #   file: '/templates/alpine.yml'
  #
  - local: '/templates/alpine.yml'
  - local: '/alpine-ci.yml'

  # Arch container builder template
  # projects using this should reference this with the following:
  #
  # - project: 'wayland/ci-templates'
  #   ref: master # or git sha, see https://docs.gitlab.com/ee/ci/yaml/#includefile
  #   file: '/templates/arch.yml'
  #
  - local: '/templates/arch.yml'
  - local: '/arch-ci.yml'

  # Centos container builder template
  # projects using this should reference this with the following:
  #
  # - project: 'wayland/ci-templates'
  #   ref: master # or git sha, see https://docs.gitlab.com/ee/ci/yaml/#includefile
  #   file: '/templates/centos.yml'
  #
  - local: '/templates/centos.yml'
  - local: '/centos-ci.yml'

  # Debian container builder template
  # projects using this should reference this with the following:
  #
  # - project: 'wayland/ci-templates'
  #   ref: master # or git sha, see https://docs.gitlab.com/ee/ci/yaml/#includefile
  #   file: '/templates/debian.yml'
  #
  - local: '/templates/debian.yml'
  - local: '/debian-ci.yml'

  # Fedora container builder template
  # projects using this should reference this with the following:
  #
  # - project: 'wayland/ci-templates'
  #   ref: master # or git sha, see https://docs.gitlab.com/ee/ci/yaml/#includefile
  #   file: '/templates/fedora.yml'
  #
  - local: '/templates/fedora.yml'
  - local: '/fedora-ci.yml'

  # Ubuntu container builder template
  # projects using this should reference this with the following:
  #
  # - project: 'wayland/ci-templates'
  #   ref: master # or git sha, see https://docs.gitlab.com/ee/ci/yaml/#includefile
  #   file: '/templates/ubuntu.yml'
  #
  - local: '/templates/ubuntu.yml'
  - local: '/ubuntu-ci.yml'

variables:
  BOOTSTRAP_TAG: '2019-12-02'


stages:
  - sanity check
  - bootstrapping
  - alpine_container_build
  - alpine_check
  - arch_container_build
  - arch_check
  - centos_container_build
  - centos_check
  - debian_container_build
  - debian_check
  - fedora_container_build
  - fedora_check
  - ubuntu_container_build
  - ubuntu_check
  - container_push


#
# We want those to fail as early as possible, so we are reusing the current
# wayland/ci-templates/buildah:latest image
#
sanity check:
  stage: sanity check
  image: $CI_REGISTRY/wayland/ci-templates/buildah:latest
  script:
    - pip3 install --user jinja2 PyYAML
    - python3 ./src/generate_templates.py

    - git diff --exit-code && exit 0 || true

    - echo "some files were not generated through 'src/generate_templates.py' or
      have not been committed. Please edit the files under 'src', run
      'src/generate_template.py' and then commit the result"
    - exit 1


check commits:
  image: $CI_REGISTRY/wayland/ci-templates/buildah:latest
  stage: sanity check
  script:
    - pip3 install GitPython
    - pip3 install pytest
    - |
      pytest --junitxml=results.xml \
             --tb=line \
             --assert=plain \
             ./.gitlab-ci/check-commit.py
  except:
    - master@wayland/ci-templates
  variables:
    GIT_DEPTH: 100
    GIT_STRATEGY: clone
  artifacts:
    reports:
      junit: results.xml


bootstrap:
  extends: .bootstrap


bootstrap@arm64v8:
  extends: .bootstrap@arm64v8


#
# Everything went fine if this is run, we can promote the latest bootstrap
# tag as latest
#
bootstrap-push@push:
  stage: container_push
  image: $CI_REGISTRY_IMAGE/buildah:$BOOTSTRAP_TAG
  script:
    # log in to the registry
    - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

    # push the container image to the registry
    - skopeo copy --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD
                  docker://$CI_REGISTRY_IMAGE/${ARCH}buildah:$BOOTSTRAP_TAG
                  docker://$CI_REGISTRY_IMAGE/${ARCH}buildah:latest
  variables:
    GIT_STRATEGY: none
  only:
    refs:
      - master


bootstrap-push@arm64v8:
  extends: bootstrap-push@push
  image: $CI_REGISTRY_IMAGE/arm64v8/buildah:$BOOTSTRAP_TAG
  before_script:
    - export ARCH="arm64v8/"
  tags:
    - aarch64
  only:
    refs:
      - master