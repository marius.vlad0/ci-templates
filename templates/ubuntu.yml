# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:

# This template will create a ubuntu image based on the following variables:
#
#  - UBUNTU_VERSION: the ubuntu version (18.10, 18.04, etc...)
#  - UBUNTU_DEBS:    if set, list of packages that needs to be installed
#  - UBUNTU_EXEC:    if set, this command will be run once the packages have
#                    been installed
#  - UPSTREAM_REPO:  the upstream project on this gitlab instance where we might
#                    find the given tag (for example: `wayland/weston`)
#  - REPO_SUFFIX:    The repository name suffix after ".../ubuntu/".
#                    If this variable isn't defined, "$UBUNTU_VERSION" is used for
#                    the suffix.
#  - UBUNTU_TAG:     tag to copy the image from the upstream registry. If the
#                    tag does not exist, create a new build and tag it
#
# The resulting image will be pushed in the local registry, under:
#     $CI_REGISTRY_IMAGE/ubuntu/$REPO_SUFFIX:$UBUNTU_TAG
#
# Two flavors of templates are available:
#   - `.ubuntu@container-build`: this will force rebuild a new container
#     and tag it with $UBUNTU_TAG without checks
#   - `.ubuntu@container-ifnot-exists`: this will rebuild a new container
#     only if $UBUNTU_TAG is not available in the local registry or
#     in the $UPSTREAM_REPO registry

# we can not reuse exported variables in after_script,
# so have a common definition
.ubuntu_vars: &distro_vars |
        # exporting templates variables
        # https://gitlab.com/gitlab-com/support-forum/issues/4349
        export BUILDAH_FORMAT=docker
        export DISTRO=ubuntu
        export DISTRO_TAG=$UBUNTU_TAG
        export DISTRO_VERSION=$UBUNTU_VERSION
        export DISTRO_EXEC=$UBUNTU_EXEC
        if [ x"$REPO_SUFFIX" == x"" ] ;
        then
                export REPO_SUFFIX=$DISTRO_VERSION
        fi
        export BUILDAH_RUN="buildah run --isolation chroot"
        export BUILDAH_COMMIT="buildah commit --format docker"


.ubuntu@container-build:
  image: $CI_REGISTRY/wayland/ci-templates/buildah:latest
  stage: build
  before_script:
    # log in to the registry
    - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

    - *distro_vars

  script:
  - *distro_vars
  - if [[ x"$DISTRO_TAG" == x"" ]] ;
    then
      echo $DISTRO tag missing;
      exit 1;
    fi
  - echo Building $DISTRO/$REPO_SUFFIX:$DISTRO_TAG from $DISTRO:$DISTRO_VERSION
    # initial set up: take the base image, update it and install the packages
  - buildcntr=$(buildah from $DISTRO:$DISTRO_VERSION)
  - buildmnt=$(buildah mount $buildcntr)

  - echo 'path-exclude=/usr/share/doc/*' > $buildmnt/etc/dpkg/dpkg.cfg.d/99-exclude-cruft
  - echo 'path-exclude=/usr/share/locale/*' >> $buildmnt/etc/dpkg/dpkg.cfg.d/99-exclude-cruft
  - echo 'path-exclude=/usr/share/man/*' >> $buildmnt/etc/dpkg/dpkg.cfg.d/99-exclude-cruft
  - echo 'APT::Install-Recommends "false";' > $buildmnt/etc/apt/apt.conf
  - echo '#!/bin/sh' > $buildmnt/usr/sbin/policy-rc.d
  - echo 'exit 101' >> $buildmnt/usr/sbin/policy-rc.d
  - chmod +x $buildmnt/usr/sbin/policy-rc.d

  - $BUILDAH_RUN $buildcntr env DEBIAN_FRONTEND=noninteractive apt-get update
  - $BUILDAH_RUN $buildcntr env DEBIAN_FRONTEND=noninteractive apt-get -y dist-upgrade

  - if [[ x"$UBUNTU_DEBS" != x"" ]] ;
    then
      $BUILDAH_RUN $buildcntr env DEBIAN_FRONTEND=noninteractive apt-get install -y $UBUNTU_DEBS ;
    fi

    # check if there is an optional post install script and run it
  - if [[ x"$DISTRO_EXEC" != x"" ]] ;
    then
      echo Running $DISTRO_EXEC ;
      set -x ;
      mkdir $buildmnt/tmp/clone ;
      pushd $buildmnt/tmp/clone ;
      git init ;
      git remote add origin $CI_REPOSITORY_URL ;
      git fetch --depth 1 origin $CI_COMMIT_SHA ;
      git checkout FETCH_HEAD  > /dev/null;
      buildah config --workingdir /tmp/clone $buildcntr ;
      $BUILDAH_RUN $buildcntr bash -c "set -x ; $DISTRO_EXEC" ;
      popd ;
      rm -rf $buildmnt/tmp/clone ;
      set +x ;
    fi

    # do not store the packages database, it's pointless
  - $BUILDAH_RUN $buildcntr env DEBIAN_FRONTEND=noninteractive apt-get clean
  - $BUILDAH_RUN $buildcntr rm -f /var/lib/apt/lists/*.lz4

    # set up the working directory
  - mkdir $buildmnt/app
  - buildah config --workingdir /app $buildcntr
    # umount the container, not required, but, heh
  - buildah unmount $buildcntr
    # tag the current container
  - $BUILDAH_COMMIT $buildcntr $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG
    # clean up the working container
  - buildah rm $buildcntr

    # push the container image to the registry
    # There is a bug when pushing 2 tags in the same repo with the same base:
    # this may fail. Just retry it after.
  - export JOB_TAG="${DISTRO_TAG}-built-by-job-${CI_JOB_ID}"
  - podman push $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG
                $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$JOB_TAG || true
  - sleep 2
  - podman push $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG
                $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$JOB_TAG || true

    # Push the final tag
  - podman push $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG || true
  - sleep 2
  - podman push $CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG

.ubuntu@container-build@arm64v8:
  extends: .ubuntu@container-build
  image: $CI_REGISTRY/wayland/ci-templates/arm64v8/buildah:latest
  tags:
    - aarch64


.before_script_ifnot_exists: &before_script_ifnot_exists
  before_script:
    # log in to the registry
    - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

    - *distro_vars

    # to be able to test the following script in the CI of the ci-templates
    # project itself, we need to put a special case here to have a
    # different image to pull if it already exists
    - export REPO_SUFFIX_LOCAL=$REPO_SUFFIX
    - if [[ x"$REPO_SUFFIX_LOCAL" == x"ci_templates_test_upstream" ]] ;
      then
        export REPO_SUFFIX=${DISTRO_VERSION} ;
      fi

    # check if our image is already in the current registry
    - skopeo inspect docker://$CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX_LOCAL:$DISTRO_TAG > /dev/null && exit 0 || true

    # check if our image is already in the upstream registry
    - skopeo inspect docker://$CI_REGISTRY/$UPSTREAM_REPO/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG > /dev/null
      && touch .upstream || true

    # copy the original image into the current project registry namespace
    # we do 2 attempts with skopeo copy at most
    - if [ -f .upstream ] ;
      then
        skopeo copy --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD
                    docker://$CI_REGISTRY/$UPSTREAM_REPO/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG
                    docker://$CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX_LOCAL:$DISTRO_TAG ||
        skopeo copy --dest-creds $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD
                    docker://$CI_REGISTRY/$UPSTREAM_REPO/$DISTRO/$REPO_SUFFIX:$DISTRO_TAG
                    docker://$CI_REGISTRY_IMAGE/$DISTRO/$REPO_SUFFIX_LOCAL:$DISTRO_TAG ;

        exit 0 ;
      fi


.ubuntu@container-ifnot-exists:
  extends: .ubuntu@container-build
  <<: *before_script_ifnot_exists


.ubuntu@container-ifnot-exists@arm64v8:
  extends: .ubuntu@container-ifnot-exists
  image: $CI_REGISTRY/wayland/ci-templates/arm64v8/buildah:latest
  tags:
    - aarch64
